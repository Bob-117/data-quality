# Data Quality Project

- [x] Bricaud Olivier
- [x] Gouriou Aurélien
- [x] Proust Stephen

## Radar

![Radar](radar/data/render_100.png)

#### Données

- Source : https://www.data.gouv.fr/fr/datasets/radars-automatiques/

- Stockage : `Postgres`

```shell
docker run --name=data_quality_radar -e POSTGRES_PASSWORD=1234 -p 5432:5432 --rm postgres:latest
psql -h 0.0.0.0 -p 5432 -U postgres --password
psql -h 0.0.0.0 -U postgres -a -f radar/data/radar.sql
```

#### Python

```py
python3.10 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

- Collecte & Traitement : `radar/src/data.py`

```py
# radar/src/data.py
DATA_LIMIT = 100
```

```shell
INFO:root:Loading 3350 entries from radars.csv, returning 100 as required.
INFO:root:Init dataframe :
   date_heure_dernier_changement  ... vitesse_vehicules_legers_kmh
0           2018-06-28T12:24:19Z  ...                          NaN
..                           ...  ...                          ...
99          2018-09-05T12:20:10Z  ...                         80.0

[100 rows x 15 columns]
INFO:root:Remove NaN in cols ['latitude', 'longitude', 'id']: from 100 to 100 entries
INFO:root:Final dataframe :
           x         y   name
0   49.11907  6.204080  12923
..       ...       ...    ...
99  47.96500 -0.582000  13696

[100 rows x 3 columns]

INFO:root:Creating Database Instance
INFO:root:Database Instance Creation Success
INFO:root:Saving 100 items in Postgres
INFO:root:Nb items loaded back from Postgres : 200

```

- Disponibilite : `radar/src/api.py`, accès au données sur l'endpoint `/radar`

```shell
curl 127.0.0.1:8001/radar

# [{"x":49.11907,"y":6.20408,"name":12923}, {"x":47.93,"y":-1.9418,"name":13619}, ....{"x":47.93,"y":-1.9418,"name":13619}]
```

- Affichage : `src/display.html`, utilisation de `leaflet` (https://leafletjs.com/examples/quick-start/)


## Replication

- https://cloudinfrastructureservices.co.uk/setup-mariadb-replication/

- https://mariadb.com/kb/en/setting-up-replication/#setting-up-a-replication-slave-with-mariabackup


```sh
# On ajoute nano à l'image officielle pour modifier les fichiers de configuration
docker build -t mariadb-with-nano -f db_replication/Dockerfile .
```

```sh
docker network create mariadb_master_net
docker network create mariadb_slave_net
```

```sh
docker run --name mariadb_master -e MYSQL_ROOT_PASSWORD=root_master -p 3307:3306 --network mariadb_master_net mariadb-with-nano
docker run --name mariadb_slave  -e MYSQL_ROOT_PASSWORD=root_slave  -p 3308:3306 --network mariadb_slave_net  mariadb-with-nano
```

```sh
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mariadb_master
# 172.20.0.2
```

### Replication Master

```sh
mysql -h 0.0.0.0 -P 3307 -u root -p
```

```sql
CREATE USER 'bob_replication'@'%' IDENTIFIED BY '1234';
GRANT REPLICATION SLAVE ON *.* TO 'bob_replication'@'%';
FLUSH PRIVILEGES;
```

```sh
docker exec -it mariadb_master bash
nano /etc/mysql/mariadb.conf.d/50-server.cnf

[mariadb]
log_bin = /var/lib/mysql/mariadb-bin
binlog_format = ROW
# server-id              = 1
# bind-address           = 0.0.0.0
# max_binlog_size        = 256M
```

```sh
docker restart mariadb_master && docker logs -f mariadb_master
```

```sh
mysql> SHOW MASTER STATUS;
+--------------------+----------+--------------+------------------+
| File               | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+--------------------+----------+--------------+------------------+
| mariadb-bin.000001 |     1909 |              |                  |
+--------------------+----------+--------------+------------------+
1 row in set (0,00 sec)
```

### Replication Slave

```sh
docker exec -it mariadb_slave mysql -u root -p
```

```sql
STOP SLAVE;

CHANGE MASTER TO
  MASTER_HOST='172.20.0.2',
  MASTER_PORT=3306,
  MASTER_USER='bob_replication',
  MASTER_PASSWORD='1234',
  MASTER_LOG_FILE='mariadb-bin.000001',
  MASTER_LOG_POS=1909;

START SLAVE;
```

```sql
SHOW SLAVE STATUS \G
*************************** 1. row ***************************
                Slave_IO_State: Connecting to master
                   Master_Host: 172.20.0.2
                   Master_User: bob_replication
                   Master_Port: 3306
                 Connect_Retry: 60
               Master_Log_File: mariadb-bin.000001
           Read_Master_Log_Pos: 1909
                Relay_Log_File: mysqld-relay-bin.000001
                 Relay_Log_Pos: 4
         Relay_Master_Log_File: mariadb-bin.000001
              Slave_IO_Running: Connecting
             Slave_SQL_Running: Yes
          Replicate_Rewrite_DB: 
               Replicate_Do_DB: 
           Replicate_Ignore_DB: 
            Replicate_Do_Table: 
        Replicate_Ignore_Table: 
       Replicate_Wild_Do_Table: 
   Replicate_Wild_Ignore_Table: 
                    Last_Errno: 0
                    Last_Error: 
                  Skip_Counter: 0
           Exec_Master_Log_Pos: 1909
               Relay_Log_Space: 256
               Until_Condition: None
                Until_Log_File: 
                 Until_Log_Pos: 0
            Master_SSL_Allowed: No
            Master_SSL_CA_File: 
            Master_SSL_CA_Path: 
               Master_SSL_Cert: 
             Master_SSL_Cipher: 
                Master_SSL_Key: 
         Seconds_Behind_Master: NULL
 Master_SSL_Verify_Server_Cert: No
                 Last_IO_Errno: 0
                 Last_IO_Error: 
                Last_SQL_Errno: 0
                Last_SQL_Error: 
   Replicate_Ignore_Server_Ids: 
              Master_Server_Id: 0
                Master_SSL_Crl: 
            Master_SSL_Crlpath: 
                    Using_Gtid: No
                   Gtid_IO_Pos: 
       Replicate_Do_Domain_Ids: 
   Replicate_Ignore_Domain_Ids: 
                 Parallel_Mode: optimistic
                     SQL_Delay: 0
           SQL_Remaining_Delay: NULL
       Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
              Slave_DDL_Groups: 0
Slave_Non_Transactional_Groups: 0
    Slave_Transactional_Groups: 0
1 row in set (0,00 sec)
```

### Back to Replication Master

```sh
CREATE DATABASE replication_db;
CREATE TABLE items (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
);
INSERT INTO your_table_name (name) VALUES ('Gibson');
INSERT INTO your_table_name (name) VALUES ('Fender');
```

### On Replication Slave

```sh
mysql> select * from replication_db.items;
+----+--------+
| id | name   |
+----+--------+
|  1 | Gibson |
|  2 | Fender |
+----+--------+
3 rows in set (0,00 sec)
```


### Préconisations

La replication de la donnée garantit le principe de `Disponibilité` de la donnée, et peut permettre la validation de l'`Intégrité` de la donnée (en comparant deux source par exemple).

Il permet d'anticiper la perte d'une machine ou d'un équipement de stockage. Nous pouvons envisager deux cas concrets :

- La panne/accident/usure d'une machine
- L'attaque cyber

Ainsi la replication presente de nombreux enjeux :

- Matériel et Réseau 

Dans le cas présenté ici, avoir deux docker sur une meme machine, meme si sur deux docker network différents, ne reflete pas la réalité et représente une manipulation technique dans un environnement connu et maitrisé afin de mettre en application le cours. Cependant, nous recommendont la mise en place d'une replication de données sur plusieurs réseaux / sous réseaux différents afin d'isoler au mieux des éléments du systeme d'information suspects ou corrompus.

- Securité

La gestion des utilisateurs ayant accès a la donnée (utilisateur db, applicatif...) ne doit pas etre "root:root". La configuration des ports par défaut est a changer, des vues sql doivent etre utilisées en surcouche des tables.

- Gestion (Planification, Erreurs, Surveillance)

La perte ou la corruption de données est un danger pour les entreprises. L'anticipation d'un tel evenement est cruciale. Des choix techniques doivent etre fait en amont. Les flux sur le SI et les accès aux données doivent etre surveillés (log api, log db, IDS). La `Tracabilité` de la donnée est une pratique recommandée afin de maitriser les enjeux légaux et métiers.

- Évolutivité

La mise en place de la réplication s'accompagne de choix techniques dépendant du cas d'usage, du type et du volume de données, et necessite une certaine projection dans le temps afin d'anticiper des sourcis de scalabilité ou d'accès a la données.

- Restauration

Des procédures de restauration de la donnée / d'un systeme d'information doivent être réfléchis afin d'avoir les outils necessaires à la remise en état du systeme dans un temps raisonnable et dans de bonnes conditions de reprise de l'activité (au mieux, la réplication peut permettre d'empecher un arret de l'activité).

- RGPD

Lors d'une violation de données, le RGPD incombe à un Responsable de Traitement sa déclaration dans les 72h. 

- Memo Data

```txt
Trois deux un : 1 donnée, 2 supports, 3 endroits physiques
```