CREATE TABLE IF NOT EXISTS radar (
           id SERIAL PRIMARY KEY,
           x FLOAT,
           y FLOAT,
           name CHAR(255)
       );
