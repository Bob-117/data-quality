import logging

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from radar.src.pg_database import PostgresDatabase


def create_api(database):
    logging.info('Creating api')
    _app = FastAPI()

    origins = [
        "http://localhost",
        "http://127.0.0.1:8000",
        "file://home/bob/epsi_i1/data_quality/src"
    ]

    _app.add_middleware(
        CORSMiddleware,
        allow_origins='*',
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @_app.get('/')
    def root():
        return 'gibson'

    @_app.get('/radar')
    def radars():
        logging.info('SELECT * FROM SQL')
        res = database.select_all_sql()
        _radars = []
        for row in res:
            dict_row = dict(row._asdict())
            _radars.append(dict_row)
        return _radars

    return _app


def process():
    try:
        logging.info(f'Creating Database Instance')
        _db = PostgresDatabase()
    except Exception as e:
        log_msg = str(e.__cause__).replace('\n', '')
        logging.info(f'Error connecting database')
        logging.info(f'{log_msg}')
    else:
        api = create_api(database=_db)
        uvicorn.run(api, port=8001)


if __name__ == '__main__':
    process()
