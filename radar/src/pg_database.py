import logging

from sqlalchemy import create_engine, text, Connection
from sqlalchemy.exc import OperationalError

logging.getLogger('Postgres Database')
logging.basicConfig(level=logging.DEBUG)

TABLE = 'radar'


class PostgresDatabase:
    def __init__(self):
        self.conn_string = 'postgresql://postgres:1234@localhost:5432/postgres'
        self.engine = create_engine(self.conn_string)
        self.__valid_connection()

    def __valid_connection(self):
        try:
            with self.engine.connect() as connection:
                assert isinstance(connection, Connection)
        except OperationalError as sql_alc_err:
            # logging.error(sql_alc_err)
            raise sql_alc_err

    def select_all_sql(self):
        with self.engine.connect() as con:
            stmt = f"""SELECT * FROM {TABLE}"""
            result = con.execute(text(stmt))
            rows = result.fetchall()
        return rows
        # logging.info('From Postgres :')
        # for row in rows:
        #     logging.info(row)
        #     yield row

    def prepared_stmt(self):
        connection = self.engine.connect()
        sql = 'SELECT * FROM my_table WHERE id = :id'
        stmt = text(sql)
        res = connection.execute(stmt, {'id': 123})
        logging.info(f'Executed SQL {sql} : {res}')
        connection.close()
        return res

    def dataframe_to_postgres(self, _dataframe, table=TABLE):
        connection = self.engine.connect()
        # Save only common cols between df and sql table (x, y, name)
        _index = False
        # we could also use df["latitude", "longitude", "id"]
        _dataframe.to_sql(table, self.engine, if_exists='append', index=_index)
        connection.close()
