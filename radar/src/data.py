# std
import logging
import os

# data
import csv
import pandas as pd

from radar.src.pg_database import PostgresDatabase

# Const
DATA_SOURCE = os.path.join(os.path.dirname(__file__), '../data/radars.csv')
DATA_WANTED = ["latitude", "longitude", "id"]
DATA_LIMIT = 100

logging.basicConfig(level=logging.INFO)


# Data methods with pandas
def read_raw_data():
    with open(DATA_SOURCE, newline='\n') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            print(row)


def __csv_data_to_df(_limit=False):
    """
    Load csv data in pandas dataframe
    Can use bool param to limit 100 entries
    """
    df = pd.read_csv(DATA_SOURCE)
    logging.info(f'Loading {len(df)} entries from {os.path.basename(DATA_SOURCE)}'
                 +
                 f", returning {DATA_LIMIT} as required." if _limit else "")
    return (df[0:DATA_LIMIT]) if _limit else df


def __remove_none(df):
    """
    Remove row with NaN
    :param df:
    :return:
    """
    before = len(df)
    cols_to_check = DATA_WANTED
    df_without_none = df.dropna(subset=cols_to_check, thresh=len(cols_to_check))
    logging.info(f'Remove NaN in cols {cols_to_check}: from {before} to {len(df_without_none)} entries')
    return df_without_none


def process():
    # Data preprocessing
    _df = __csv_data_to_df(_limit=True)
    logging.info(f'Init dataframe :\n{_df}')
    _df = _df[["latitude", "longitude", "id"]]
    _df_without_nan = __remove_none(_df)
    _coord_df = _df_without_nan[DATA_WANTED]
    _final = _coord_df.rename(columns={'latitude': 'x', 'longitude': 'y', 'id': 'name'})
    logging.info(f'Final dataframe :\n{_final}')
    # Data registration
    try:
        logging.info(f'Creating Database Instance')
        _db = PostgresDatabase()
    except Exception as e:
        log_msg = str(e.__cause__).replace('\n', '')
        logging.info(f'Error connecting database')
        logging.info(f'{log_msg}')
    else:
        logging.info(f'Database Instance Creation Success')
        # Save in Database
        logging.info(f'Saving {len(_final)} items in Postgres')
        _db.dataframe_to_postgres(_final)
        # Load from Database
        logging.info(f'Nb items loaded back from Postgres : {len(list(_db.select_all_sql()))}')


if __name__ == '__main__':
    process()
